#pragma once

#include "Form3D.h"

namespace infographie
{
	class IcoSphere : public Form3D
	{
	private:
		static int nbOfIcoSphere;
		const string BASE_NAME = "IcoSphere ";

	protected:
		float radius;
		int iteration;

	public:
		IcoSphere();
		IcoSphere(int, string = "");
		virtual ~IcoSphere();

		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) override;
	};
}

