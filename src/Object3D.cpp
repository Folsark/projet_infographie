#include "Object3D.h"
#include "Sphere.h"
#include "Plane.h"
#include "Box.h"
#include "IcoSphere.h"
#include "Cylindre.h"
#include "Cone.h"
#include "Model3D.h"

using namespace infographie;

shared_ptr<Object3D> Object3D::Make3DObject(Objec3DType type, int nbToInstanciate, string name)
{
	switch (type)
	{
	case Objec3DType::ICO_SPHERE:
		return make_shared<IcoSphere>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::SPHERE:
		return make_shared<Sphere>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::CONE:
		return make_shared<Cone>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::CYLINDRE:
		return make_shared<Cylindre>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::PLANE:
		return make_shared<Plane>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::BOX:
		return make_shared<Box>(nbToInstanciate, name); // 1float 1int
	case Objec3DType::MODEL:
		return make_shared<Model3D>(nbToInstanciate, name);
	default:
		return nullptr;
	}
}

void Object3D::DrawBoundingBox()
{
	ofPushMatrix();
	ofNoFill();
	//ofRotate(degree, vector_rotation.x, vector_rotation.y, vector_rotation.z);
	//ofDrawBox(vector_position, width, height, depth);
	ofPopMatrix();
}

void Object3D::Selected(bool isSel)
{
	isSelected = isSel;
}

string Object3D::GetName()
{
	return ObjectName;
}

void Object3D::SetRenderMode(MeshRenderMode mode)
{
	mesh_render_mode = mode;
}

Object3D::Object3D(int nbInstance)
{
	numberToInstanciate = nbInstance;
}


Object3D::~Object3D()
{
	delete Creator;
}
