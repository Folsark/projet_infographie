#include "Box.h"

using namespace infographie;

int Box::nbOfBox = 0;

Box::Box()
{}

Box::Box(int nbOfInstance, string name)
	:Form3D(nbOfInstance)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfBox++);
	}
	this->ObjectName = name;
	Creator = new ofxDatGui();
	Creator->addHeader("Box Creator");
	Creator->addTextInput("Width", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Height", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Depth", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addButton("Create")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	Creator->onButtonEvent(this, &Box::onCreatorButtonEvent);
	Creator->setPosition((ofGetWidth() - Creator->getWidth()) / 2, (ofGetHeight() - Creator->getHeight()) / 2);
}

Box::~Box()
{
}

void Box::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
	width = stoi(Creator->getTextInput("Width")->getText());
	height = stoi(Creator->getTextInput("Height")->getText());
	depth = stoi(Creator->getTextInput("Depth")->getText());
	xSpacing = width *2;
	Creator->setAutoDraw(false);
	this->primitives = ofBoxPrimitive(width, height, depth);
	computeBoundingBox();
	isInstanciated = true;
}

