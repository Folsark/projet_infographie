#include "Renderer.h"

void Renderer::setup()
{
	ofSetFrameRate(60);
	//ofSetBackgroundColor(191);

	//mousePosition = make_shared<ofPoint>(ofPoint());
	mousePressedPosition = ofPoint();

	activeDrawer = make_shared<Drawer3D>();	
	activeDrawer->SetAppState(appState);
	drawers.insert(pair<DrawerMode, shared_ptr<Drawer>>(DrawerMode::MODEL3D, activeDrawer));

	activeDrawer = make_shared<VectorialDrawer>();
	activeDrawer->SetAppState(appState);
	drawers.insert(pair<DrawerMode, shared_ptr<Drawer>>(DrawerMode::VECTORIAL, activeDrawer));

	activeDrawer = make_shared<ImageDrawer>();
	activeDrawer->SetAppState(appState);
	drawers.insert(pair<DrawerMode, shared_ptr<Drawer>>(DrawerMode::IMAGE, activeDrawer));
	activeDrawer->setup();
	activeDrawer->isEnable(true);

	SetupGUI();
}

void Renderer::SetAppState(shared_ptr<AppState> appState)
{
	this->appState = appState;
}

void Renderer::SetupGUI()
{
	modeSelector = new ofxDatGui(ofxDatGuiAnchor::BOTTOM_MIDDLE, true);
	modeSelector->setWidth(ofGetWidth()/4);
	selectedModeName = modeSelector->addTextInput("Selected Mode", "Image");
	for (pair<std::string, DrawerMode> p : DrawerModeNameToEnum)
	{
		modeSelector->addButton(p.first)->setIndex(static_cast<int>(p.second));
	}

	modeSelector->onButtonEvent(this, &Renderer::onModeSelectionButtonEvent);
}

void Renderer::onModeSelectionButtonEvent(ofxDatGuiButtonEvent e)
{
	std::string name = e.target->getName();
	activeDrawer->isEnable(false);
	activeDrawer = drawers.find(DrawerModeNameToEnum.find(name)->second)->second;
	if (!activeDrawer->IsSetup()) activeDrawer->setup();
	activeDrawer->isEnable(true);
	selectedModeName->setText(name);

}

void Renderer::update()
{
	activeDrawer->update();
}

void Renderer::removeSelected()
{
	activeDrawer->removeSelected();
}

void Renderer::draw()
{
	//ofClear(255);
	// afficher la zone de s�lection
	activeDrawer->draw();
	if (appState->isMouseDown && activeDrawer->DrawSelectorSquare())
	{
		ofSetColor(255);
		ofSetLineWidth(3);
		ofNoFill();

		draw_zone(
			mousePressedPosition.x,
			mousePressedPosition.y,
			appState->mousePosition.x,
			appState->mousePosition.y);
	}
	
	// afficher le curseur
	ofSetLineWidth(2);
	//ofSetColor(31);

	draw_cursor(appState->mousePosition.x, appState->mousePosition.y);
}

void Renderer::SetMousePosition(shared_ptr<ofPoint> p)
{
	//mousePosition = p;
}

void Renderer::SetMousePressedPosition(ofPoint p)
{
	mousePressedPosition = p; 
	activeDrawer->MousePressed(p);
}


void Renderer::SetMouseReleasedPosition(ofPoint p)
{
	activeDrawer->MouseReleased(p);
}

void Renderer::Selection()
{
	//for(shared_ptr<Primitive> p:primitives)
}

// fonction qui vide le tableau de primitives vectorielles
void Renderer::reset()
{
	ofLog() << "<reset>";
}

void Renderer::draw_zone(float x1, float y1, float x2, float y2) const
{
	float x2_clamp = min(max(0.0f, x2), (float)ofGetWidth());
	float y2_clamp = min(max(0.0f, y2), (float)ofGetHeight());

	ofDrawRectangle(x1, y1, x2_clamp - x1, y2_clamp - y1);
}

void Renderer::draw_cursor(float x, float y) const
{
	float length = 10.0f;
	float offset = 5.0f;
	ofSetColor(31);
	ofDrawLine(x + offset, y, x + offset + length, y);
	ofDrawLine(x - offset, y, x - offset - length, y);
	ofDrawLine(x, y + offset, x, y + offset + length);
	ofDrawLine(x, y - offset, x, y - offset - length);
	ofSetColor(255);
}

Renderer::~Renderer()
{
	delete modeSelector;
}
