#include "Form3D.h"

using namespace infographie;

void Form3D::draw()
{
	if (!isInstanciated)return;

	int half = floor(((float)numberToInstanciate)/2);
	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2, 0);

	ofEnableDepthTest(); // enable depth test

	primitives.setPosition(
		vector_position.x,
		vector_position.y,
		vector_position.z);

	switch (mesh_render_mode)
	{
	case MeshRenderMode::FILL:
		primitives.draw(OF_MESH_FILL);
		break;

	case MeshRenderMode::WIREFRAME:
		primitives.draw(OF_MESH_WIREFRAME);
		break;

	case MeshRenderMode::VERTEX:
		primitives.draw(OF_MESH_POINTS);
		break;
	}

	if (isSelected) DrawBoundingBox();


	ofDisableDepthTest(); // disable depth test

	ofPopMatrix();
}

Form3D::Form3D(int nbOfInstance)
	:Object3D(nbOfInstance)
{
	center_x = ofGetWidth() / 2.0f;
	center_y = ofGetHeight() / 2.0f;

	speed = 100;
}

Form3D::~Form3D()
{
	delete Creator;
}

void infographie::Form3D::computeBoundingBox()
{
	ofMesh mesh = primitives.getMesh();
	vector<ofPoint>vertices = mesh.getVertices();

	for (ofPoint v : vertices)
	{
		min.x = (v.x < min.x) ? v.x - 10 : min.x;
		min.y = (v.y < min.y) ? v.y - 10 : min.y;
		min.z = (v.z < min.z) ? v.z - 10 : min.z;
		max.x = (v.x > max.x) ? v.x + 10 : max.x;
		max.y = (v.y > max.y) ? v.y + 10 : max.y;
		max.z = (v.z > max.z) ? v.z + 10 : max.z;
	}
	objCenter = primitives.getPosition();

	width = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;
}

void Form3D::Rotate(float degree, ofVec3f rotation)
{
	ofPushMatrix();
	this->degree = degree;
	//ofRotate(degree, rotation.x, rotation.y, rotation.z);
	primitives.rotate(degree, rotation);
	ofPopMatrix();
	vector_rotation = rotation;
}

void Form3D::Translate(ofVec3f translation)
{
	vector_position += translation;
	//ofPushMatrix();
	//ofTranslate(translation);
	//primitives.setPosition(primitives.getPosition() + translation);
	//ofPopMatrix();
}


