#pragma once
#include "Form3D.h"

namespace infographie
{
	class Sphere : public Form3D
	{
	private:
		static int nbOfSphere;
		const string BASE_NAME = "Sphere ";

	protected:
		float radius;
		int resolution;

	public:

	protected:

	public:
		Sphere();
		Sphere(int, string = "");
		virtual ~Sphere();

		// Inherited via Form3D
		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) override;

	};
}
