#include "Model3D.h"

using namespace infographie;

int Model3D::nbOfModel = 0;

void Model3D::draw()
{
	if (!isInstanciated)return;

	int half = floor(((float)numberToInstanciate) / 2);
	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2, 0);

	ofEnableDepthTest(); // enable depth test
	for(int i = 0; i < numberToInstanciate; i++)
	{
		ofPushMatrix();

		vector_position.x = locators[i].position[0];
		vector_position.y = locators[i].position[1];
		vector_position.z = locators[i].position[2];


		model.setPosition(
			vector_position.x,
			vector_position.y,
			vector_position.z);

		vector_rotation.x = locators[i].rotation[0];
		vector_rotation.y = locators[i].rotation[1];
		vector_rotation.z = locators[i].rotation[2];


		model.setRotation(0.0f, vector_rotation.y, 0.0f, 1.0f, 0.0f);


		vector_proportion.x = locators[i].proportion[0];
		vector_proportion.y = locators[i].proportion[1];
		vector_proportion.z = locators[i].proportion[2];

		model.setScale(
			vector_proportion.x,
			vector_proportion.y,
			vector_proportion.z);

		if (isSelected) DrawBoundingBox();

		switch (mesh_render_mode)
		{
		case MeshRenderMode::FILL:
			model.draw(OF_MESH_FILL);
			break;

		case MeshRenderMode::WIREFRAME:
			model.draw(OF_MESH_WIREFRAME);
			break;

		case MeshRenderMode::VERTEX:
			model.draw(OF_MESH_POINTS);
			break;
		}

		ofPopMatrix();
	}
	ofDisableDepthTest(); // disable depth test

	ofPopMatrix();
}

void Model3D::computeBoundingBox()
{
	ofMesh mesh = model.getMesh(0);
	vector<ofPoint>vertices = mesh.getVertices();

	for (ofPoint v : vertices)
	{
		min.x = (v.x < min.x) ? v.x - 10 : min.x;
		min.y = (v.y < min.y) ? v.y - 10 : min.y;
		min.z = (v.z < min.z) ? v.z - 10 : min.z;
		max.x = (v.x > max.x) ? v.x + 10 : max.x;
		max.y = (v.y > max.y) ? v.y + 10 : max.y;
		max.z = (v.z > max.z) ? v.z + 10 : max.z;
	}

	width = max.x - min.x;
	height = max.y - min.y;
	depth = max.z - min.z;
	objCenter = model.getSceneCenter();
}

// fonction qui distribue les localisateurs dans un espace cubique
void Model3D::dispatch_random_locator(float range)
{
	float scale;

	float halfRange = range / 2.0f;

	for (int index = 0; index < numberToInstanciate; ++index)
	{
		Locator locator;

		vector_position.x = ofRandom(-halfRange, halfRange);
		vector_position.y = ofRandom(-halfRange, halfRange);
		vector_position.z = ofRandom(-halfRange, halfRange);

		vector_rotation.x = 0.0f;
		vector_rotation.y = ofRandom(0.0f, 360.0f);
		vector_rotation.z = 0.0f;

		scale = ofRandom(0.05f, 0.35f);

		vector_proportion.x = scale;
		vector_proportion.y = scale;
		vector_proportion.z = scale;

		// configurer les attributs de transformation du teapot
		locator.position[0] = vector_position.x;
		locator.position[1] = vector_position.y;
		locator.position[2] = vector_position.z;

		locator.rotation[0] = vector_rotation.x;
		locator.rotation[1] = vector_rotation.y;
		locator.rotation[2] = vector_rotation.z;

		locator.proportion[0] = vector_proportion.x;
		locator.proportion[1] = vector_proportion.y;
		locator.proportion[2] = vector_proportion.z;

		locators.push_back(locator);
	}
}


Model3D::Model3D(int nbInstance, string name)
	:Object3D(nbInstance)
{
	model.loadModel(name);
	size_t startPos = name.find(".obj");
	if (startPos != string::npos) name.replace(name.begin() + startPos, name.end(), to_string(nbOfModel++) +".obj");
	else 
	{
		startPos = name.find(".fbx");
		if (startPos != string::npos) name.replace(name.begin() + startPos, name.end(), to_string(nbOfModel++) + ".fbx");
	}
	this->ObjectName = name;
	computeBoundingBox();
	dispatch_random_locator((ofGetWidth() * 0.6f<ofGetHeight() * 0.6f)? ofGetWidth() * 0.6f : ofGetHeight() * 0.6f);
	isInstanciated = true;
}


Model3D::~Model3D()
{
}

void infographie::Model3D::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
}

void Model3D::Rotate(float degree, ofVec3f rotation)
{
	ofPushMatrix();
	//degree = model.getRotationAngle(0) + degree;
	//vector_rotation += rotation;
	for (int i = 0; i < locators.size(); i++)
	{
		locators[i].rotation[0] += rotation.x;
		locators[i].rotation[1] += rotation.y;
		locators[i].rotation[2] += rotation.z;
		//model.setRotation(i, model.getRotationAngle(0) + degree, vector_rotation.x, vector_rotation.y, vector_rotation.z);
	}
	ofPopMatrix();
}

void Model3D::Translate(ofVec3f translation)
{
	vector_position += translation;
	for (int i = 0; i < locators.size(); i++)
	{
		locators[i].position[0] += translation.x;
		locators[i].position[1] += translation.y;
		locators[i].position[2] += translation.z;
		//model.setRotation(i, model.getRotationAngle(0) + degree, vector_rotation.x, vector_rotation.y, vector_rotation.z);
	}
	/*ofPushMatrix();
	model.setPosition(vector_position.x, vector_position.y, vector_position.z);
	ofPopMatrix();*/
}
