#pragma once

#include "ofMain.h"
//#include "GeometricalForm.h"
#include "ImageDrawer.h"
#include "Drawer3D.h"
#include "Utils.h"
#include "VectorialDrawer.h"
#include <typeinfo>
#include "ofxDatGui\ofxDatGui.h"

using namespace infographie;

class Renderer
{
private:
	ofPoint mousePressedPosition;
	shared_ptr<AppState> appState;

	ofxDatGui* modeSelector;
	ofxDatGuiTextInput* selectedModeName;

	shared_ptr<Drawer> activeDrawer;
	map<DrawerMode, shared_ptr<Drawer>> drawers;

public :

private:
	void SetupGUI();

public:
	void removeSelected();
	void onModeSelectionButtonEvent(ofxDatGuiButtonEvent e);

	void SetMousePosition(shared_ptr<ofPoint>);
	void SetMousePressedPosition(ofPoint);
	void SetMouseReleasedPosition(ofPoint);
	void Selection();

	void update();
	void setup();
	void SetAppState(shared_ptr<AppState>);
	void draw();

	void reset();

	void draw_zone(float x1, float y1, float x2, float y2) const;
	void draw_cursor(float x, float y) const;

	Renderer() = default;
	~Renderer();
};
