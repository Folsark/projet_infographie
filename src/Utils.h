#pragma once

namespace infographie {

	/**
	* General
	**/

	typedef struct AppState {
		ofPoint mousePosition;
		ofPoint deltaMouse;
		bool isMouseDown;
		bool isShiftDown;
	}AppState;

	/**
	* Image
	**/

	enum class DrawingTool
	{
		SELECTION = 0,
		PEN = 1,
		CROPPER = 2,
		COLOR_PICKER = 3
	};

	const map<std::string, DrawingTool> DrawingToolsName = {
		{ "Selection", DrawingTool::SELECTION },
		{ "Pixel", DrawingTool::PEN },
		{ "Crop", DrawingTool::CROPPER },
		{ "Color Picker", DrawingTool::COLOR_PICKER}
	};

	/**
	* Vectorial
	**/

	enum class Tool
	{
		SELECTION = 0,
		DRAW_POLYLINE = 1,
		DRAW_RECTANGLE = 2,
		DRAW_ELLIPS = 3,
		DRAW_REGULAR_POLYGONE = 4,
		TRANSLATION = 5,
		ROTATION = 6
	};

	const map<std::string, Tool> ToolsName = {
		{ "Selection", Tool::SELECTION },
		{ "Polyline", Tool::DRAW_POLYLINE },
		{ "Rectangle", Tool::DRAW_RECTANGLE },
		{ "Ellipse", Tool::DRAW_ELLIPS },
		{ "Polygone", Tool::DRAW_REGULAR_POLYGONE },
		{ "Rotation", Tool::ROTATION },
		{ "Translation", Tool::TRANSLATION }
	};

	/**
	* Renderer
	**/

	enum class DrawerMode
	{
		IMAGE = 0,
		VECTORIAL = 1,
		MODEL3D = 2
		};

	const map<std::string, DrawerMode> DrawerModeNameToEnum = {
		{ "Image", DrawerMode::IMAGE },
		{ "Dessin vectoriel", DrawerMode::VECTORIAL },
		{ "model 3D", DrawerMode::MODEL3D }
	};

	/**
	* 3D 
	**/
	enum class MeshRenderMode { 
		FILL = 0,
		WIREFRAME = 1, 
		VERTEX=2 
	};

	const map<std::string, MeshRenderMode> MeshRenderModeName = {
		{ "Filled", MeshRenderMode::FILL },
		{ "Wireframe", MeshRenderMode::WIREFRAME },
		{ "Vertex", MeshRenderMode::VERTEX }
	};

	struct Locator
	{
		float position[3];   // 3 * 4 = 12 octets
		float rotation[3];   // 3 * 4 = 12 octets
		float proportion[3]; // 3 * 4 = 12 octets
	};                     //       = 36 octets

	enum class TransformationTool
	{
		SELECTION = 0,
		ROTATION = 1,
		TRANSLATION = 2
	};

	const map<std::string, TransformationTool> TransformationToolsName = {
		{ "Selection", TransformationTool::SELECTION },
		{ "Rotation", TransformationTool::ROTATION },
		{ "Translation", TransformationTool::TRANSLATION }
	};

	enum class Objec3DType
	{
		CYLINDRE = 0,
		PLANE = 1,
		BOX = 2,
		SPHERE = 3,
		CONE = 4,
		ICO_SPHERE = 5,
		MODEL = 6
	};

	const map<std::string, Objec3DType> Object3DName = {
		{ "Cylindre", Objec3DType::CYLINDRE },
		{ "Plane", Objec3DType::PLANE },
		{ "Box", Objec3DType::BOX },
		{ "Sphere", Objec3DType::SPHERE },
		{ "Cone", Objec3DType::CONE },
		{ "Ico Sphere", Objec3DType::ICO_SPHERE },
		{ "Load Model", Objec3DType::MODEL }
	};

}


