#pragma once

#include "Primitive.h"
#include <vector>

namespace infographie
{
	class Polyline : public Primitive
	{
	private:
		static int nbOfPolyline;
		vector<ofPoint> ptsList;

	public:
		const std::string BASE_NAME = "Polyline ";

	protected:
		virtual void SetOfPathValue() override;
		//virtual bool isInForm(const ofPoint &) override;

	public:
		void AddPoint(ofPoint&);

		virtual void ComputeUsefullValue(ofPoint &p, bool isShiftPressd = false) override;

		Polyline(ofPoint&, ofColor, ofColor, int, std::string name = "");
		virtual ~Polyline();
	};
}

