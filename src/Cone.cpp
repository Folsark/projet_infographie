#include "Cone.h"

using namespace infographie;

int Cone::nbOfCone = 0;

Cone::Cone()
{
}

Cone::~Cone()
{
}

Cone::Cone(int nbOfInstance, string name)
	:Form3D(nbOfInstance)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfCone++);
	}
	this->ObjectName = name;
	Creator = new ofxDatGui();
	Creator->addHeader("Box Creator");
	Creator->addTextInput("Radius", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Height", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Radius Segment", "20")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Height Segment", "10")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addButton("Create")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	Creator->onButtonEvent(this, &Cone::onCreatorButtonEvent);
	Creator->setPosition((ofGetWidth() - Creator->getWidth()) / 2, (ofGetHeight() - Creator->getHeight()) / 2);
}

void Cone::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
	radius = stoi(Creator->getTextInput("Radius")->getText());
	height = stoi(Creator->getTextInput("Height")->getText());
	radiusSegment = stoi(Creator->getTextInput("Radius Segment")->getText());
	heightSegment = stoi(Creator->getTextInput("Height Segment")->getText());
	xSpacing = radius * 3;
	Creator->setAutoDraw(false);
	this->PrimitiveCreation();
	computeBoundingBox();
	isInstanciated = true;
}

void Cone::PrimitiveCreation()
{
	this->primitives = ofConePrimitive(radius, height, radiusSegment, heightSegment);
}