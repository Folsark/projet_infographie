#pragma once
#include "ofMain.h"
#include "string.h"
#include "Utils.h"

namespace infographie 
{
	class Primitive
	{
	protected:
		std::string name;
		ofPath primitive;
		ofRectangle boundingBox;
		ofVec3f rotation;
		float angle;
		ofPoint minPoint, maxPoint;

		ofPoint position;

	public:
		const float PERFECT_PROPORTION_MULTIPLIER = 1.6f;
		const float BASE_LENGTH = 10;

		ofColor strokeColor, filledColor;
		int strokeThickness;

	protected:
		virtual void SetOfPathValue() = 0;

	public:
		static shared_ptr<Primitive> MakePrimitive(Tool, ofPoint&, ofColor = ofColor(0,0,0), ofColor = ofColor(0, 0, 0), int = 1, int = 3);

		virtual void ComputeUsefullValue(ofPoint &p, bool isShiftPressd = false) = 0;

		bool isInside(ofPoint&);

		std::string GetName() const;
		void SetName(std::string);

		//virtual bool isInForm(const ofPoint &) = 0;

		bool Rotate(float az, const ofVec3f &axis);
		bool Translate(const ofPoint &p);
		bool MoveTo(const ofPoint &p);
		bool MoveTo(float, float, float);

		void SetFilledColor(ofColor);
		void SetStrokeColor(ofColor);
		void SetStrokeThickness(int);

		ofColor GetFilledColor();
		ofColor GetStrokeColor();
		int GetStrokeThickness();

		void Draw();
		void DrawBoundingBox();
		void UpdateBoundingBox();

		Primitive& operator=(const Primitive &);
		virtual int GetNumbrOfVertices();

		Primitive() = default;
		Primitive(ofPoint&, ofColor, ofColor, int);
		Primitive(ofPoint &, ofColor, ofColor, int, std::string name);
		Primitive(const Primitive&);
		virtual ~Primitive() = default;
	};
}

