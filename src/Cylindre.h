#pragma once

#include "Cone.h"

namespace infographie
{
	class Cylindre : public Cone
	{
	private:
		static int nbOfCylindre;
		const string BASE_NAME = "Cylindre ";

	protected:
		void PrimitiveCreation();

	public:
		Cylindre();
		Cylindre(int, string = "");
		virtual ~Cylindre();
	};
}
