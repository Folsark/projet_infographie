#include "Plane.h"

using namespace infographie;

int Plane::nbOfPlane = 0;

Plane::Plane()
{
}

Plane::~Plane()
{
}

Plane::Plane(int nbOfInstances, string name)
	:Form3D(nbOfInstances)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfPlane++);
	}
	this->ObjectName = name;
	Creator = new ofxDatGui();
	Creator->addHeader("Box Creator");
	Creator->addTextInput("Width", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Height", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Column", "30")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Row", "30")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addButton("Create")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	Creator->onButtonEvent(this, &Plane::onCreatorButtonEvent);
	Creator->setPosition((ofGetWidth() - Creator->getWidth()) / 2, (ofGetHeight() - Creator->getHeight()) / 2);
}

void Plane::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
	width = stoi(Creator->getTextInput("Width")->getText());
	height = stoi(Creator->getTextInput("Height")->getText()); 
	column = stoi(Creator->getTextInput("Column")->getText());
	row = stoi(Creator->getTextInput("Row")->getText());
	xSpacing = width * 3;
	Creator->setAutoDraw(false);
	this->primitives = ofPlanePrimitive(width, height, column, row);
	computeBoundingBox();
	isInstanciated = true;
}