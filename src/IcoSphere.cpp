#include "IcoSphere.h"

using namespace infographie;

int IcoSphere::nbOfIcoSphere = 0;

IcoSphere::IcoSphere()
{
}

IcoSphere::IcoSphere(int nbOfInstances, string name)
	:Form3D(nbOfInstances)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfIcoSphere++);
	}
	this->ObjectName = name;
	Creator = new ofxDatGui();
	Creator->addHeader("Box Creator");
	Creator->addTextInput("Radius", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Iteration", "2")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addButton("Create")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	Creator->onButtonEvent(this, &IcoSphere::onCreatorButtonEvent);
	Creator->setPosition((ofGetWidth() - Creator->getWidth()) / 2, (ofGetHeight() - Creator->getHeight()) / 2);
}

void IcoSphere::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
	radius = stoi(Creator->getTextInput("Radius")->getText());
	iteration = stoi(Creator->getTextInput("Iteration")->getText());
	xSpacing = radius * 3;
	Creator->setAutoDraw(false);
	this->primitives = ofIcoSpherePrimitive(radius, iteration);
	computeBoundingBox();
	isInstanciated = true;
}

IcoSphere::~IcoSphere()
{
}