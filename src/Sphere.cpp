#include "Sphere.h"

using namespace infographie;

int Sphere::nbOfSphere = 0;

Sphere::Sphere()
{
}

Sphere::Sphere(int nbOfInstances, string name)
	:Form3D(nbOfInstances)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfSphere++);
	}
	this->ObjectName = name;

	Creator = new ofxDatGui();
	Creator->addHeader("Box Creator");
	Creator->addTextInput("Radius", "100")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addTextInput("Resolution", "20")->setInputType(ofxDatGuiInputType::NUMERIC);
	Creator->addButton("Create")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	Creator->onButtonEvent(this, &Sphere::onCreatorButtonEvent);
	Creator->setPosition((ofGetWidth() - Creator->getWidth()) / 2, (ofGetHeight() - Creator->getHeight()) / 2);
}

Sphere::~Sphere()
{
	delete Creator;
}

void Sphere::onCreatorButtonEvent(ofxDatGuiButtonEvent e)
{
	radius = stoi(Creator->getTextInput("Radius")->getText());
	resolution = stoi(Creator->getTextInput("Resolution")->getText());
	xSpacing = radius * 3;
	Creator->setAutoDraw(false);
	this->primitives = ofSpherePrimitive(radius, resolution);
	computeBoundingBox();
	isInstanciated = true;
}