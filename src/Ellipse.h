#pragma once

#include "Primitive.h"

namespace infographie
{
	class Ellipse : public Primitive
	{
	private:
		static int nbOfEllipse;
		float width, height;

	public:
		const std::string BASE_NAME = "Ellipse ";

	protected:
		virtual void SetOfPathValue() override;
		//virtual bool isInForm(const ofPoint &) override;

	public:
		Ellipse & operator=(Ellipse const &);

		virtual void ComputeUsefullValue(ofPoint &p, bool isShiftPressd = false) override;

		Ellipse(ofPoint&, ofColor, ofColor, int, std::string name = "");
		virtual ~Ellipse();
	};
}

