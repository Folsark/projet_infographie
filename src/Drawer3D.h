#pragma once

#include "Drawer.h"
#include "ofxAssimpModelLoader.h"
#include "Object3D.h"

namespace infographie
{
	class Drawer3D : public Drawer
	{
	private:
		map<string, shared_ptr<Object3D>> forms;
		shared_ptr<Object3D> selectedForm;

		TransformationTool tool;

		ofLight light;

		ofxDatGui* ToolSelector;
		ofxDatGui* ToolOption;
		ofxDatGui* FileOption;
		ofxDatGui* ModelLoader;

		bool updateFileList = false;

	public:

	private:
		void NewModel();

		void DrawMode();

	public:
		void onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e);
		void onModelCreationButtonEvent(ofxDatGuiButtonEvent e);
		void onFileOptionButtonEvent(ofxDatGuiButtonEvent e);

		void Create(int, string ="");

		// Inherited via Drawer
		virtual void MousePressed(ofPoint) override;

		virtual void update() override;
		virtual void draw() override;
		virtual void setup() override;

		// Inherited via Drawer
		virtual bool DrawSelectorSquare() override;
		virtual void Load(std::string) override;
		virtual void Save(std::string) override;

		// Inherited via Drawer
		virtual void isEnable(bool) override;

		// Inherited via Drawer
		virtual void MouseReleased(ofPoint) override;

		// Inherited via Drawer
		virtual void SetupGUI() override;

		// Inherited via Drawer
		virtual void reset() override;

		virtual void removeSelected() override;
	
		Drawer3D();
		virtual ~Drawer3D();
	};
}

