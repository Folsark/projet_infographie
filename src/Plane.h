#pragma once

#include "Form3D.h"

namespace infographie
{
	class Plane : public Form3D
	{
	private:
		float width, height;
		int column, row;

		static int nbOfPlane;
		const string BASE_NAME = "Plane ";

	public:
		Plane();
		Plane(int, string ="");
		virtual ~Plane();

		// Inherited via Form3D
		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) override;
	};
}

