#include "RegularPolygone.h"

using namespace infographie;

int RegularPolygone::nbOfRegPoly = 0;

void RegularPolygone::SetOfPathValue()
{
	this->primitive.clear();
	this->primitive.setFillColor(filledColor);
	this->primitive.setStrokeColor(strokeColor);
	this->primitive.setStrokeWidth(strokeThickness);

	this->primitive.moveTo(this->listOfPoint[0]);
	for (int i = 1; i < this->listOfPoint.size(); i++)
	{
		this->primitive.lineTo(this->listOfPoint[i]);
	}
	this->primitive.close();
}

void RegularPolygone::ComputeUsefullValue(ofPoint & p, bool isShiftPressd)
{
	this->minPoint = ofPoint(p);
	this->maxPoint = ofPoint(p);

	this->listOfPoint.clear();
	this->listOfPoint.push_back(p);
	ofPoint delta = p - this->position;
	float radius = this->position.distance(p);
	float initAngle = (delta).angleRad(ofVec3f(1, 0, 0));
	float angleOffset = 2.0f*PI / (nbOfVertices);
	
	for (float i = angleOffset; i < 2.0f * PI; i += angleOffset)
	{
		ofLog() << i * 180 / PI;
		ofPoint tmp;
		tmp.y = this->position.y + delta.y*cos(i) - delta.x*sin(i);
		tmp.x = this->position.x + delta.y*sin(i) + delta.x*cos(i);
		this->listOfPoint.push_back(tmp);

		this->maxPoint.x = (this->maxPoint.x < tmp.x) ? tmp.x : this->maxPoint.x;
		this->maxPoint.y = (this->maxPoint.y < tmp.y) ? tmp.y : this->maxPoint.y;
		this->minPoint.x = (this->minPoint.x > tmp.x) ? tmp.x : this->minPoint.x;
		this->minPoint.y = (this->minPoint.y > tmp.y) ? tmp.y : this->minPoint.y;

	}
	SetOfPathValue();
	UpdateBoundingBox();
}

int RegularPolygone::GetNumbrOfVertices()
{
	return nbOfVertices;
}

RegularPolygone& RegularPolygone::operator=(const RegularPolygone& rp)
{
	this->listOfPoint.clear();
	this->listOfPoint = vector<ofPoint>(rp.listOfPoint);
	this->nbOfVertices = rp.nbOfVertices;
	return *this;
}

RegularPolygone::RegularPolygone(ofPoint& p, int nbOfVertices, ofColor filledColor, ofColor strokeColor, int strokeThickness, std::string name)
	:Primitive(p, filledColor, strokeColor, strokeThickness)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfRegPoly++);
	}
	this->name = name;
	this->nbOfVertices = nbOfVertices;
	this->listOfPoint = vector<ofPoint>();
}

RegularPolygone::~RegularPolygone()
{
}
