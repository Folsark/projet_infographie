#pragma once

#include "ofxDatGui\ofxDatGui.h"
#include "Utils.h"

namespace infographie
{
	class Object3D
	{
	protected:
		bool isInstanciated = false;
		int numberToInstanciate;

		string ObjectName;

		MeshRenderMode mesh_render_mode;

		ofBoxPrimitive boundingBox;

		ofxDatGui* Creator;

		ofPoint min, max, objCenter;
		float width, height, depth;

		float speed;

		float degree;
		ofVec3f vector_position;
		ofVec3f vector_rotation;
		ofVec3f vector_proportion;

		float center_x;
		float center_y;

		float offset_x;
		float offset_z;

		float delta_x;
		float delta_z;

		bool is_flip_axis_y;

		bool is_active_translation;
		bool is_active_rotation;
		bool is_active_proportion;

		bool isSelected;

		virtual void computeBoundingBox() = 0;

	public:
		void Selected(bool);

		virtual void Rotate(float degree, ofVec3f rotation) = 0;
		virtual void Translate(ofVec3f) = 0;

		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) = 0;

		static shared_ptr<Object3D> Make3DObject(Objec3DType, int, string = "");

		void SetRenderMode(MeshRenderMode);
		string GetName();

		void DrawBoundingBox();

		virtual void draw() = 0;

		Object3D(int);
		virtual ~Object3D();
	};


}