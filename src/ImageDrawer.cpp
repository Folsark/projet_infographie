#include "ImageDrawer.h"

using namespace infographie;

void ImageDrawer::MousePressed(ofPoint p)
{
	mousePressed = p;
}


void ImageDrawer::removeSelected()
{

}

void ImageDrawer::update()
{

	if(imageModified)GUI->getTextInput("file name")->setLabelColor(ofColor::red);
	else GUI->getTextInput("file name")->setLabelColor(ofColor::green);

	if (updateFileList) 
	{
		vector<string> fileList;
		ofDirectory tmp = ofDirectory(filesystem::path(""));
		tmp.allowExt("png");
		tmp.listDir();
		for (ofFile f : tmp.getFiles())
		{
			fileList.push_back(f.getFileName());
		}
		fileListDd->UpdateOption(fileList);
		updateFileList = false;
	}

	GUI->getTextInput("Pen Size")->setVisible(tool == DrawingTool::PEN);

	if (appState->isMouseDown && image.isAllocated())
	{
		if (tool == DrawingTool::COLOR_PICKER)
		{
			selectedColor = image.getColor(appState->mousePosition.x, appState->mousePosition.y);
			colorPicker->setColor(selectedColor);
		}else if (tool == DrawingTool::PEN)
		{
			for (int i = -penSize / 2; i < penSize / 2; i++) 
			{
				for (int j= -penSize / 2; j < penSize / 2; j++) 
				{
					image.setColor(appState->mousePosition.x + i, appState->mousePosition.y + j, selectedColor);
				}
			}
			image.update();
			imageModified = true;
		}
	}
}

void ImageDrawer::draw()
{
	if (image.isAllocated()) image.draw(0,0);
}

void ImageDrawer::setup()
{
	SetupGUI();

	isSetup = true;
}

void ImageDrawer::SetupGUI()
{
	ToolSelector = new ofxDatGui(ofxDatGuiAnchor::MIDDLE_RIGHT); 
	for (pair<std::string, DrawingTool> p : DrawingToolsName)
	{
		ToolSelector->addButton(p.first)->setIndex(static_cast<int>(p.second));
	}
	ToolSelector->setWidth(75, .9f);
	ToolSelector->onButtonEvent(this, &ImageDrawer::onToolsSelectionButtonEvent);

	GUI = new ofxDatGui(ofxDatGuiAnchor::TOP_MIDDLE, true);
	GUI->setWidth(150, .5f);
	GUI->addTextInput("file name", fileName);
	vector<string> filesOption = vector<string>({"new image", "load image", "save image"});
	ofxDatGuiFolder* folder = GUI->addFolder("Files");
	folder->addButton("new image");
	folder->addButton("load image");
	folder->addButton("save image");
	//GUI->addButton("List File")->setIndex(2);
	colorPicker = GUI->addColorPicker("color");
	GUI->addTextInput("Pen Size")->setInputType(ofxDatGuiInputType::NUMERIC);
	GUI->onButtonEvent(this, &ImageDrawer::onButtonEvent);
	GUI->onTextInputEvent(this, &ImageDrawer::onTextInputEvent);
	GUI->onColorPickerEvent(this, &ImageDrawer::onColorPickerEvent);

	this->FileLoader = new ofxDatGui();
	FileLoader->addHeader("Image List");
	vector<string> fileList;
	ofDirectory tmp = ofDirectory(filesystem::path(""));
	tmp.allowExt("png");
	tmp.listDir();
	for (ofFile f : tmp.getFiles())
	{
		fileList.push_back(f.getFileName());
	}
	fileListDd = FileLoader->addDropdown("Images", fileList);
	FileLoader->addButton("Ok")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	FileLoader->addButton("Cancel")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	FileLoader->setPosition((ofGetWidth() - FileLoader->getWidth()) / 2, (ofGetHeight() - FileLoader->getHeight()) / 2);
	FileLoader->onButtonEvent(this, &ImageDrawer::onFileSelectionButtonEvent);
	FileLoader->setAutoDraw(false);

	isEnable(false);
}

void infographie::ImageDrawer::reset()
{
}


void ImageDrawer::onFileSelectionButtonEvent(ofxDatGuiButtonEvent e)
{
	if (e.target->is("Ok"))
	{
		fileName = FileLoader->getDropdown("Images")->getSelected()->getLabel();
		GUI->getTextInput("file name")->setText(fileName);
		Load(fileName);
	}
	FileLoader->setAutoDraw(false);
}

void ImageDrawer::onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e)
{
	std::string name = e.target->getName();
	tool = DrawingToolsName.find(e.target->getName())->second;
}

void ImageDrawer::onTextInputEvent(ofxDatGuiTextInputEvent e)
{
	if (e.target->is("file name"))
	{
		fileName = e.target->getName();
		if (fileName.find(".png") != std::string::npos)
		{
			fileName += ".png";
			GUI->getTextInput("file name")->setText(fileName);
		}
	}
	else if (e.target->is("Pen Size"))
	{
		penSize = stoi(e.target->getText());
	}
}

void ImageDrawer::onColorPickerEvent(ofxDatGuiColorPickerEvent e)
{
	selectedColor = e.target->getColor();
}

void ImageDrawer::onButtonEvent(ofxDatGuiButtonEvent e)
{

	ofLog() << "button clicked " << e.target->getName();
	if (e.target->is("new image"))
	{
		NewImage();
	}
	else if (e.target->is("save image"))
	{
		Save(fileName);
	}
	else if (e.target->is("load image"))
	{
		FileLoader->setAutoDraw(true);
	}
	if (GUI->getFolder("Files")->getIsExpanded()) GUI->getFolder("Files")->collapse();
}

void ImageDrawer::NewImage()
{
	image.clear();
	fileName = "newImage.png"; 
	GUI->getTextInput("file name")->setText(fileName);
	//GUI->getTextInput("file name")->setLabelColor(ofColor::red);
	image.allocate(ofGetWidth(), ofGetHeight(), ofImageType::OF_IMAGE_COLOR);
	updateFileList = true;
	imageModified = true;
}

void ImageDrawer::Load(std::string name)
{
	ofFile dir = ofFile(filesystem::path(name), ofFile::Mode::ReadWrite);
	if (dir.exists()) ofLog() << "file " << fileName << " exist";
	image.clear();
	image.load(dir);
	dir.close();
}

void ImageDrawer::Save(std::string name)
{
	ofFile dir = ofFile(filesystem::path(name), ofFile::Mode::ReadWrite);
	image.save(dir, ofImageQualityType::OF_IMAGE_QUALITY_MEDIUM);
	dir.close();
	imageModified = true;
}

bool infographie::ImageDrawer::DrawSelectorSquare()
{
	return tool == DrawingTool::SELECTION;
}

void infographie::ImageDrawer::isEnable(bool enable)
{

	if (enable) ofLog() << "enable Image UI";
	else ofLog() << "disable Image UI";
	GUI->setAutoDraw(enable);
	ToolSelector->setAutoDraw(enable);
}

void infographie::ImageDrawer::MouseReleased(ofPoint)
{
	//if (!image.isAllocated()) return;
	//ofImage tmp = image;
	//image.cropFrom(image, 100, 100, 300, 300);
}

ImageDrawer::ImageDrawer()
{
}


ImageDrawer::~ImageDrawer()
{
	if (isSetup)
	{
		delete GUI;
		delete ToolSelector;
		delete FileLoader;
	}
}