#include "Drawer3D.h"

using namespace infographie;

void Drawer3D::MousePressed(ofPoint)
{
}

void Drawer3D::update()
{
	if (!isSetup) return;
	if (appState->isMouseDown)
	{
		if (tool == TransformationTool::ROTATION)
		{
			ofLog() << "rotation";
			if (selectedForm != nullptr) selectedForm->Rotate(1, appState->deltaMouse);
		}
		else if (tool == TransformationTool::TRANSLATION)
		{
			ofLog() << "translation";
			if (selectedForm != nullptr) selectedForm->Translate(appState->deltaMouse);
		}
	}
}

void Drawer3D::draw()
{
	for (pair<string, shared_ptr<Object3D>> f : forms) f.second->draw();
	//if (selectedForm != nullptr) selectedForm->DrawBoundingBox();
}

void Drawer3D::setup()
{
	// paramètres de rendu
	ofSetFrameRate(60);
	SetupGUI();

	reset();
	isSetup = true;
}

void Drawer3D::DrawMode()
{
	/*switch (mesh_render_mode)
	{
	case MeshRenderMode::fill:
		model.draw(OF_MESH_FILL);
		break;

	case MeshRenderMode::wireframe:
		model.draw(OF_MESH_WIREFRAME);
		break;

	case MeshRenderMode::vertex:
		model.draw(OF_MESH_POINTS);
		break;
	}*/
}

bool Drawer3D::DrawSelectorSquare()
{
	return tool == TransformationTool::SELECTION;
}

void Drawer3D::NewModel()
{
}

void Drawer3D::Load(std::string modelName)
{
	//model.loadModel(modelName);
}

void Drawer3D::Save(std::string)
{
}

void Drawer3D::isEnable(bool enable)
{/*
	if (enable) ofEnableDepthTest();
	else ofDisableDepthTest();*/

	ToolSelector->setAutoDraw(enable);
	ToolOption->setAutoDraw(enable);
	FileOption->setAutoDraw(enable);
}

void Drawer3D::MouseReleased(ofPoint)
{
}

void Drawer3D::removeSelected()
{
	forms.erase(selectedForm->GetName());
	selectedForm = nullptr;
}

void Drawer3D::SetupGUI()
{
	this->ToolSelector = new ofxDatGui(ofxDatGuiAnchor::MIDDLE_RIGHT);
	this->ToolSelector->addTextInput("Tool : ");
	for (pair<std::string, TransformationTool> p : TransformationToolsName)
	{
		this->ToolSelector->addButton(p.first)->setIndex(static_cast<int>(p.second));
	}
	this->ToolSelector->setWidth(100);
	this->ToolSelector->onButtonEvent(this, &Drawer3D::onToolsSelectionButtonEvent);


	ToolOption = new ofxDatGui(ofxDatGuiAnchor::TOP_MIDDLE, true);
	ToolOption->addColorPicker("model Color");

	this->FileOption = new ofxDatGui(ofxDatGuiAnchor::MIDDLE_LEFT);
	FileOption->addTextInput("Object name");
	//ofxDatGuiFolder* folder = FileOption->addFolder("New model");
	vector<string> opt;
	for (pair<std::string, Objec3DType> p : Object3DName)
	{
		opt.push_back(p.first);
	}
	FileOption->addDropdown("Object to Create", opt);

	FileOption->addButton("Instanciate");
	FileOption->onButtonEvent(this, &Drawer3D::onFileOptionButtonEvent);

	this->ModelLoader = new ofxDatGui();
	ModelLoader->addHeader("Model List");
	ModelLoader->addTextInput("number of model", "1")->setInputType(ofxDatGuiInputType::NUMERIC);

	vector<string> fileList;
	ofDirectory tmp = ofDirectory(filesystem::path(""));
	tmp.allowExt("obj");
	tmp.listDir();
	for (ofFile f : tmp.getFiles())
	{
		fileList.push_back(f.getFileName());
	}
	tmp.allowExt("fbx");
	tmp.listDir();
	for (ofFile f : tmp.getFiles())
	{
		fileList.push_back(f.getFileName());
	}
	ModelLoader->addDropdown("models", fileList);
	ModelLoader->addButton("Ok")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	ModelLoader->addButton("Cancel")->setLabelAlignment(ofxDatGuiAlignment::CENTER);
	ModelLoader->setPosition((ofGetWidth() - ModelLoader->getWidth()) / 2, (ofGetHeight() - ModelLoader->getHeight()) / 2);
	ModelLoader->onButtonEvent(this, &Drawer3D::onModelCreationButtonEvent);
	ModelLoader->setAutoDraw(false);
	isEnable(false);
}


void Drawer3D::onModelCreationButtonEvent(ofxDatGuiButtonEvent e)
{
	if (e.target->is("Ok"))
	{
		Create(stoi(ModelLoader->getTextInput("number of model")->getText()), ModelLoader->getDropdown("models")->getSelected()->getLabel());
	}
	ModelLoader->setAutoDraw(false);
}

void Drawer3D::onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e)
{
	std::string name = e.target->getName();
	tool = TransformationToolsName.find(e.target->getName())->second;
	this->ToolSelector->getTextInput("Tool : ")->setText(name);
}

void Drawer3D::onFileOptionButtonEvent(ofxDatGuiButtonEvent e)
{
	if (e.target->is("Load Model"))
	{

	}
	else if (e.target->is("Instanciate"))
	{
		if (FileOption->getDropdown("Object to Create")->getSelected()->getLabel().compare("Load Model") == 0)
		{
			vector<string> fileList;
			ofDirectory tmp = ofDirectory(filesystem::path(""));
			tmp.allowExt("obj");
			tmp.listDir();
			for (ofFile f : tmp.getFiles())
			{
				fileList.push_back(f.getFileName());
			}
			tmp.allowExt("fbx");
			tmp.listDir();
			for (ofFile f : tmp.getFiles())
			{
				fileList.push_back(f.getFileName());
			}
			ModelLoader->getDropdown("models")->UpdateOption(fileList);
			ModelLoader->setAutoDraw(true);
		}
		else Create(1);
	}
}

void Drawer3D::Create(int nbOfModel, string modelName)
{
	if (selectedForm != nullptr) selectedForm->Selected(false);
	selectedForm = Object3D::Make3DObject(Object3DName.find(FileOption->getDropdown("Object to Create")->getSelected()->getLabel())->second, nbOfModel, modelName);
	selectedForm->Selected(true);
	forms.insert(pair<string, shared_ptr<Object3D>>(selectedForm->GetName(), selectedForm));
	selectedForm->SetRenderMode(MeshRenderMode::FILL);
	FileOption->getTextInput("Object name")->setText(selectedForm->GetName());
}

void Drawer3D::reset()
{
	ofEnableLighting();

	light.setDiffuseColor(ofColor(255, 255, 255));
	light.setPosition(0.0f, 0.0f, 1000.0f);
	light.enable();
}

Drawer3D::Drawer3D()
{
}


Drawer3D::~Drawer3D()
{
	if (isSetup)
	{
		delete ToolSelector;
		delete FileOption;
		delete ToolOption;
		delete ModelLoader;
	}
}
