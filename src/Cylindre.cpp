#include "Cylindre.h"

using namespace infographie;

int Cylindre::nbOfCylindre = 0;

Cylindre::Cylindre()
{
}

Cylindre::Cylindre(int nbOfInstances, string name)
	:Cone(nbOfInstances)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfCylindre++);
	}
	this->ObjectName = name;
}

Cylindre::~Cylindre()
{
}

void Cylindre::PrimitiveCreation()
{
	this->primitives = ofCylinderPrimitive(radius, height, radiusSegment, heightSegment);
}