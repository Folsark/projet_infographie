#pragma once
#include "Object3D.h"
#include "ofxAssimpModelLoader.h"

namespace infographie
{
	class Model3D : public Object3D
	{
	private:
		ofxAssimpModelLoader model;
		string modelName;

		vector<Locator> locators;

		static int nbOfModel;

	private:
		virtual void computeBoundingBox() override;
		void dispatch_random_locator(float range);

	public:

		virtual void draw() override;

		Model3D(int, string);
		virtual ~Model3D();

		// Inherited via Object3D
		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) override;

		virtual void Rotate(float degree, ofVec3f rotation) override;
		virtual void Translate(ofVec3f) override;
	};
}

