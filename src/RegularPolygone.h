#pragma once
#include "Primitive.h"

namespace infographie
{
	class RegularPolygone : public Primitive
	{
	private:
		static int nbOfRegPoly;
		int nbOfVertices = 3;
		vector<ofPoint> listOfPoint;

	public:
		const std::string BASE_NAME = "Polygone ";
		//virtual bool isInForm(const ofPoint &) override;
	private:

	public:
		// Inherited via Primitive
		virtual void SetOfPathValue() override;
		virtual void ComputeUsefullValue(ofPoint & p, bool isShiftPressd = false) override;

		RegularPolygone& operator=(const RegularPolygone&);

		virtual int GetNumbrOfVertices() override;

		RegularPolygone(ofPoint&, int, ofColor, ofColor, int = 1, std::string = "");

		virtual ~RegularPolygone();

	};
}



