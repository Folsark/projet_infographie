#pragma once

#include "of3dPrimitives.h"
#include "ofMain.h"
#include "Utils.h"
#include "Object3D.h"
#include "ofxDatGui\ofxDatGui.h"

namespace infographie
{
	class Form3D : public Object3D
	{
	protected:
		of3dPrimitive primitives;

		float xSpacing;

		//ofxAssimpModelLoader model;
	public:

	protected:

	public:

		virtual void draw() override;

		Form3D(int = 1);
		virtual ~Form3D();

		// Inherited via Object3D
		virtual void computeBoundingBox() override;

		// Inherited via Object3D
		virtual void Rotate(float degree, ofVec3f rotation) override;
		virtual void Translate(ofVec3f) override;
	};
}

