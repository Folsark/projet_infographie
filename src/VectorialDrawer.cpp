#include "VectorialDrawer.h"

using namespace infographie;

void VectorialDrawer::setup()
{
	selectedPrimitives = vector<shared_ptr<Primitive>>();
	primitives = map<string, shared_ptr<Primitive>>();

	tool = Tool::SELECTION;

	mStrokeThickness = 2;

	random_color_stroke();
	random_color_fill();

	SetupGUI();
	isSetup = true;
}

void VectorialDrawer::draw()
{
	for (pair<string, shared_ptr<Primitive>> p : primitives) {
		p.second->Draw();
	}
	for (shared_ptr<Primitive> p : selectedPrimitives)
		p->DrawBoundingBox();
}

void VectorialDrawer::update()
{
	if(selectedPrimitives.size() != 0 && this->appState->isMouseDown && inCreation) selectedPrimitives[0]->ComputeUsefullValue(appState->mousePosition, appState->isShiftDown);
	if (selectedPrimitives.size() == 1 && inCreation) regularPolyVerticesNumber->setVisible(tool == Tool::DRAW_REGULAR_POLYGONE || typeid(*selectedPrimitives[0]) == typeid(infographie::RegularPolygone));

	if (appState->isMouseDown)
	{
		if (tool == Tool::ROTATION)
		{
			ofLog() << "rotation";
			for (shared_ptr<Primitive> p : selectedPrimitives)
				p->Rotate(1, appState->deltaMouse);
		}
		else if (tool == Tool::TRANSLATION)
		{
			ofLog() << "translation";
			for (shared_ptr<Primitive> p : selectedPrimitives)
				p->Translate(appState->deltaMouse);
		}
	}
}

void VectorialDrawer::SetupGUI()
{
	toolSelectorGUI = new ofxDatGui(ofxDatGuiAnchor::MIDDLE_RIGHT);
	for (pair<std::string, Tool> p : ToolsName)
	{
		toolSelectorGUI->addButton(p.first)->setIndex(static_cast<int>(p.second));
	}
	toolSelectorGUI->setWidth(75, .9f);
	toolSelectorGUI->onButtonEvent(this, &VectorialDrawer::onToolsSelectionButtonEvent);

	toolsOptionGUI = new ofxDatGui(ofxDatGuiAnchor::TOP_MIDDLE, true);

	primitiveName = toolsOptionGUI->addTextInput("Name");
	primitiveName->setWidth(150, .4f);
	regularPolyVerticesNumber = toolsOptionGUI->addTextInput("Vertices", to_string(mRegPolyNbVertices));
	regularPolyVerticesNumber->setInputType(ofxDatGuiInputType::NUMERIC);
	regularPolyVerticesNumber->setWidth(150, .75f);
	regularPolyVerticesNumber->setVisible(false);
	fillColor = toolsOptionGUI->addColorPicker("Fill Color", mFilledColor);
	fillColor->setWidth(150, .7f);
	strokeColor = toolsOptionGUI->addColorPicker("Stroke color", mStrokeColor);
	strokeColor->setWidth(150, .7f);
	strokeThickness = toolsOptionGUI->addTextInput("Stroke thickness", to_string(mStrokeThickness));
	strokeThickness->setWidth(150, .75f);
	strokeThickness->setInputType(ofxDatGuiInputType::NUMERIC);


	toolsOptionGUI->onColorPickerEvent(this, &VectorialDrawer::onToolsColorPickerEvt);
	toolsOptionGUI->onTextInputEvent(this, &VectorialDrawer::onToolsTextInputEvt);

	isEnable(false);
}

void VectorialDrawer::onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e)
{
	std::string name = e.target->getName();
	SetSelectedTool(ToolsName.find(e.target->getName())->second);
}

void VectorialDrawer::onToolsColorPickerEvt(ofxDatGuiColorPickerEvent e)
{
	if (e.target->getName().compare(strokeColor->getName()) == 0)
	{
		mStrokeColor = strokeColor->getColor();
	}
	else if (e.target->getName().compare(fillColor->getName()) == 0)
	{
		mFilledColor = fillColor->getColor();
	}
	UpdateSelectedForm();
}

void VectorialDrawer::onToolsTextInputEvt(ofxDatGuiTextInputEvent e)
{
	if (e.target->getName().compare(primitiveName->getName()) == 0)
	{
		selectedPrimitives[0]->SetName(primitiveName->getName());
	}
	else if (e.target->getName().compare(regularPolyVerticesNumber->getName()) == 0)
	{
		mRegPolyNbVertices = stoi(regularPolyVerticesNumber->getText());
	}
	else if (e.target->getName().compare(strokeThickness->getName()) == 0)
	{
		mStrokeThickness = stoi(strokeThickness->getText());
	}
	UpdateSelectedForm();
}

void VectorialDrawer::MousePressed(ofPoint p)
{
	switch (tool)
	{
		// a revoir;
	case Tool::DRAW_ELLIPS:
	case Tool::DRAW_POLYLINE:
	case Tool::DRAW_RECTANGLE:
	case Tool::DRAW_REGULAR_POLYGONE:
		ToolEffect(p);
		break;
	case Tool::SELECTION:
		if (!appState->isShiftDown) selectedPrimitives.clear();
		for (pair<string, shared_ptr<Primitive>> primitive : primitives)
		{
			if (primitive.second->isInside(p))
			{
				selectedPrimitives.push_back(primitive.second);
				if (appState->isShiftDown) break;
			}
		}
		UpdateGUI();
		break;
	default:
		break;
	}
}

void VectorialDrawer::ToolEffect(ofPoint p)
{
	if (selectedPrimitives.size() != 0) // if selected object is a polyline and in polyline mode
	{
		if (typeid(*selectedPrimitives[0]) == typeid(infographie::Polyline) && tool == Tool::DRAW_POLYLINE)
		{
			dynamic_cast<infographie::Polyline&>(*selectedPrimitives[0]).AddPoint(p);
		}
		else
		{
			selectedPrimitives.clear();
			selectedPrimitives.push_back(Primitive::MakePrimitive(this->tool, p, mFilledColor, mStrokeColor, mStrokeThickness, mRegPolyNbVertices));
			primitives.insert(pair<string, shared_ptr<Primitive>>(selectedPrimitives[0]->GetName(), selectedPrimitives[0]));
			inCreation = true;
		}
	}
	else
	{
		selectedPrimitives.push_back(Primitive::MakePrimitive(this->tool, p, mFilledColor, mStrokeColor, mStrokeThickness, mRegPolyNbVertices));
		primitives.insert(pair<string, shared_ptr<Primitive>>(selectedPrimitives[0]->GetName(), selectedPrimitives[0]));
		inCreation = true;
	}
	UpdateGUI();
}

void VectorialDrawer::UpdateSelectedForm()
{
	for (shared_ptr<Primitive> p : selectedPrimitives)
	{
		p->SetFilledColor(mFilledColor);
		//selectedPrimitives->SetName()
		p->SetStrokeColor(mStrokeColor);
		p->SetStrokeThickness(mStrokeThickness);
	}
}


void VectorialDrawer::UpdateGUI()
{
	if (selectedPrimitives.size() == 0) return;
	primitiveName->setText(selectedPrimitives[0]->GetName());
	fillColor->setColor(selectedPrimitives[0]->GetFilledColor());
	strokeColor->setColor(selectedPrimitives[0]->GetStrokeColor());
	strokeThickness->setText(to_string(selectedPrimitives[0]->GetStrokeThickness()));
	if (typeid(*selectedPrimitives[0]) == typeid(infographie::RegularPolygone)) regularPolyVerticesNumber->setText(to_string(selectedPrimitives[0]->GetNumbrOfVertices()));
}

void VectorialDrawer::SetSelectedTool(Tool tool)
{
	this->tool = tool;
	regularPolyVerticesNumber->setVisible(tool == Tool::DRAW_REGULAR_POLYGONE);
}

// fonction qui d�termine une couleur al�atoire pour les lignes de contour
void VectorialDrawer::random_color_stroke()
{
	mStrokeColor.r = (int)ofRandom(0, 256);
	mStrokeColor.g = (int)ofRandom(0, 256);
	mStrokeColor.b = (int)ofRandom(0, 256);
	mStrokeColor.a = 255;

	ofLog() << "<random color stroke>";
}

// fonction qui d�termine une couleur al�atoire pour les zones de remplissage
void VectorialDrawer::random_color_fill()
{
	mFilledColor.r = (int)ofRandom(0, 256);
	mFilledColor.g = (int)ofRandom(0, 256);
	mFilledColor.b = (int)ofRandom(0, 256);
	mFilledColor.a = 255;

	ofLog() << "<random color fill>";
}

void VectorialDrawer::removeSelected()
{
	for(shared_ptr<Primitive> p : selectedPrimitives)
	primitives.erase(p->GetName());
	selectedPrimitives.clear();
}

bool VectorialDrawer::DrawSelectorSquare()
{
	return tool == Tool::SELECTION;
}

VectorialDrawer::VectorialDrawer()
{
}

VectorialDrawer::~VectorialDrawer()
{
	if (isSetup) {
		delete toolsOptionGUI;
		delete toolSelectorGUI;
	}
}

void VectorialDrawer::Load(std::string)
{
}

void VectorialDrawer::Save(std::string)
{
}

void VectorialDrawer::isEnable(bool enable)
{
	if (enable) ofLog() << "enable Vectorial UI";
	else ofLog() << "disable Vectorial UI";
	toolSelectorGUI->setAutoDraw(enable);
	toolsOptionGUI->setAutoDraw(enable);
}

void VectorialDrawer::MouseReleased(ofPoint)
{
	if (inCreation) inCreation = false;
}

void VectorialDrawer::reset()
{
}
