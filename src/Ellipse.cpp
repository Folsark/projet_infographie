#include "Ellipse.h"

using namespace infographie;

int Ellipse::nbOfEllipse = 0;

Ellipse::Ellipse(ofPoint& p, ofColor filledColor, ofColor strokeColor, int thickness, std::string name)
	:Primitive(p, filledColor, strokeColor, thickness)
{
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfEllipse++);
	}
	this->name = name;
	this->width = BASE_LENGTH * PERFECT_PROPORTION_MULTIPLIER;
	this->height = BASE_LENGTH;

	SetOfPathValue();
}

void Ellipse::SetOfPathValue()
{
	this->primitive.clear();
	this->primitive.setFillColor(filledColor);
	this->primitive.setStrokeColor(strokeColor);
	this->primitive.setStrokeWidth(strokeThickness);
	this->primitive.ellipse(this->position, this->width, this->height);
}

void Ellipse::ComputeUsefullValue(ofPoint &p, bool isShiftPressd)
{
	this->width = (p.x - position.x);
	this->height = (p.y - position.y);

	if (isShiftPressd)
	{
		float min = abs((abs(width) < abs(height)) ? width : height);
		this->width = ((width < 0) ? -1 : 1)*min;
		this->height = ((height < 0) ? -1 : 1)*min;
	}
	
	this->minPoint.x = position.x - this->width/2;
	this->minPoint.y = position.y - this->height/2;
	this->maxPoint.x = position.x + this->width/2;
	this->maxPoint.y = position.y + +this->height/2;

	UpdateBoundingBox();
	SetOfPathValue();
}

Ellipse::~Ellipse()
{
}

infographie::Ellipse& Ellipse::operator=(const Ellipse &r)
{
	this->width = r.width;
	this->height = r.height;
	return *this;
}
