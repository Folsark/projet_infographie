#pragma once

#include "ofxDatGui\ofxDatGui.h"
#include <filesystem>

namespace infographie
{
	class FilePicker
	{
	private:
		std::string filters;
		ofDirectory openedDir;

		ofxDatGui* filePickerUI;
		ofxDatGuiFolder* dirList;

	public:

	private:
		void SetCurrentDirectories(std::string);
		void SendPathToOpen(string);
		void AddFileToUI(ofDirectory);

	public:
		std::string Open();

		FilePicker();
		~FilePicker();
	};
}

