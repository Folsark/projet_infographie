#pragma once
#include "Drawer.h"
#include "Ellipse.h"
#include "RegularPolygone.h"
#include "Rectangle.h"
#include "Polyline.h"
#include "ofxSVG.h"
#include <typeinfo>

namespace infographie
{
	class VectorialDrawer : public Drawer
	{
	private:
		map<string, shared_ptr<Primitive>> primitives;
		vector<shared_ptr<Primitive>> selectedPrimitives;

		bool inCreation;

		Tool tool;

		ofxDatGui* toolSelectorGUI;
		ofxDatGui* toolsOptionGUI;
		ofxDatGuiTextInput* primitiveName;
		ofxDatGuiColorPicker* fillColor;
		ofxDatGuiColorPicker* strokeColor;
		ofxDatGuiTextInput* strokeThickness;
		ofxDatGuiTextInput* regularPolyVerticesNumber;

		ofColor mFilledColor, mStrokeColor;
		float mStrokeThickness = 2;
		int mRegPolyNbVertices = 3;

	public:

	protected:
		virtual void SetupGUI() override;

		void UpdateSelectedForm();
		void UpdateGUI();

		void ToolEffect(ofPoint);

		void random_color_stroke();
		void random_color_fill();

	public:
		void onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e);
		void onToolsColorPickerEvt(ofxDatGuiColorPickerEvent e);
		void onToolsTextInputEvt(ofxDatGuiTextInputEvent e);

		virtual void MousePressed(ofPoint) override;

		virtual void update() override;
		virtual void setup() override;
		virtual void draw() override;

		bool DrawSelectorSquare();

		void SetSelectedTool(Tool);

		VectorialDrawer();
		virtual ~VectorialDrawer();

		// Inherited via Drawer
		virtual void Load(std::string) override;
		virtual void Save(std::string) override;

		// Inherited via Drawer
		virtual void isEnable(bool) override;

		// Inherited via Drawer
		virtual void MouseReleased(ofPoint) override;

		// Inherited via Drawer
		virtual void reset() override;

		virtual void removeSelected() override;
	};

}
