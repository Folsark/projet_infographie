#pragma once

#include "ofMain.h"
#include "Drawer.h"
#include "FilePicker.h"

namespace infographie
{
	class ImageDrawer : public Drawer
	{
	private:
		ofImage image;
		string fileName;
		float penSize;

		bool updateFileList = false;
		bool imageModified = false;

		DrawingTool tool;

		ofColor selectedColor;

		ofPoint mousePressed;
		

		ofxDatGui* ToolSelector;
		ofxDatGui* GUI;
		ofxDatGui* FileLoader;
		ofxDatGuiColorPicker* colorPicker;
		ofxDatGuiDropdown* fileListDd;

	public:

	private:
		void NewImage();

	public:
		void onToolsSelectionButtonEvent(ofxDatGuiButtonEvent e);
		void onFileSelectionButtonEvent(ofxDatGuiButtonEvent e);
		void onTextInputEvent(ofxDatGuiTextInputEvent e);
		void onColorPickerEvent(ofxDatGuiColorPickerEvent e);

		void onButtonEvent(ofxDatGuiButtonEvent e);

		ImageDrawer();
		virtual ~ImageDrawer();

		// Inherited via Drawer
		virtual void MousePressed(ofPoint) override;

		virtual void update() override;
		virtual void draw() override;
		virtual void setup() override;

		virtual void Load(std::string) override;
		virtual void Save(std::string) override;

		// Inherited via Drawer
		virtual bool DrawSelectorSquare() override;

		// Inherited via Drawer
		virtual void isEnable(bool) override;

		// Inherited via Drawer
		virtual void MouseReleased(ofPoint) override;

		void removeSelected() override;

		// Inherited via Drawer
		virtual void SetupGUI() override;

		// Inherited via Drawer
		virtual void reset() override;
	};
}

