#include "Application.h"

void Application::setup()
{
	ofSetWindowTitle("Projet Infographie");

	ofLog() << "<app::setup>";
	//cursor = make_shared<ofPoint>(ofPoint());
	ofSetEscapeQuitsApp(false);

	renderer.SetAppState(appState);
	renderer.setup();

	is_verbose = false;
}

void Application::update() {
	renderer.update();
}

void Application::draw()
{
	renderer.draw();
}

void Application::UpdateCursorPosition(float x, float y) 
{
	if (appState != nullptr)
	{
		appState->mousePosition.x = x;
		appState->mousePosition.y = y;
	}
}

void Application::mouseMoved(int x, int y)
{
	UpdateCursorPosition(x, y);
	
	if (is_verbose)
		ofLog() << "<app::mouse move at: (" << x << ", " << y << ")>";
}

void Application::mouseDragged(int x, int y, int button)
{
	if (appState != nullptr) appState->deltaMouse = ofPoint(x, y) - appState->mousePosition;
	UpdateCursorPosition(x, y);

	if (is_verbose)
		ofLog() << "<app::mouse drag at: (" << x << ", " << y << ") button:" << button << ">";
}

void Application::mousePressed(int x, int y, int button)
{
	if (ofxDatGui::hitUI(ofPoint(x, y))) return;
	appState->isMouseDown = true;
	renderer.SetMousePressedPosition(ofPoint(x, y));

	ofLog() << "<app::mouse pressed  at: (" << x << ", " << y << ")>";
}

void Application::mouseReleased(int x, int y, int button)
{
	appState->isMouseDown = false;
	renderer.SetMouseReleasedPosition(ofPoint(x, y));
	ofLog() << "<app::mouse released at: (" << x << ", " << y << ")>";
}

void Application::mouseEntered(int x, int y)
{
	cursor = make_shared<ofPoint>(ofPoint(x, y));
	renderer.SetMousePosition(cursor);

	ofLog() << "<app::mouse entered  at: (" << x << ", " << y << ")>";
}

void Application::mouseExited(int x, int y)
{
	//renderer.SetMousePosition(null);
	cursor = nullptr;
	ofLog() << "<app::mouse exited   at: (" << x << ", " << y << ")>";
}

void Application::keyReleased(int key)
{
	ofLog() << key;
	switch (key)
	{
		case 127:
		case 8:
			renderer.removeSelected();
		case 2304:
			appState->isShiftDown = false;
			break;
		default:
			break;
	}
}

void Application::keyPressed(int key)
{
	switch (key)
	{
	case 2304:
		appState->isShiftDown = true;
		break;
	default:
		
		break;
	}
}

void Application::dragEvent(ofDragInfo dragInfo) {

}
void Application::gotMessage(ofMessage msg) {

}

void Application::exit()
{
	ofLog() << "<app::exit>";
}

//--------------------------------------------------------------
void Application::windowResized(int w, int h) 
{

}

Application::Application()
{
	appState = make_shared<AppState>(AppState{ ofPoint(), ofPoint(),false, false });
}


Application::~Application()
{
}
