#pragma once
#include "Primitive.h"

namespace infographie
{
	class Rectangle : public Primitive
	{
	private:
		static int nbOfRectangle;
		float width, height;

	public:
		const std::string BASE_NAME = "Rectangle ";

	protected:
		virtual void SetOfPathValue() override;/*
		virtual bool isInForm(const ofPoint &) override;*/
	public:
		Rectangle & operator=(Rectangle const &);

		virtual void ComputeUsefullValue(ofPoint &p, bool isShiftPressd = false) override;

		Rectangle(ofPoint&, ofColor, ofColor, int, std::string name = "");
		virtual ~Rectangle();
	};
}