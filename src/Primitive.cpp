#include "Primitive.h"
#include "Ellipse.h"
#include "RegularPolygone.h"
#include "Rectangle.h"
#include "Polyline.h"

using namespace infographie;

shared_ptr<Primitive> Primitive::MakePrimitive(Tool selectedTool, ofPoint &p, ofColor filledColor, ofColor strokeColor, int strokeThickness, int nbOfPoint)
{
	switch (selectedTool)
	{
		case Tool::DRAW_ELLIPS:
			ofLog() << "Create Ellips";
			return make_shared<Ellipse>(p, filledColor, strokeColor, strokeThickness);
		case Tool::DRAW_POLYLINE:
			ofLog() << "Create Polyline";
			return make_shared<Polyline>(p, filledColor, strokeColor, strokeThickness);
		case Tool::DRAW_RECTANGLE:
			ofLog() << "Create Rectangle";
			return make_shared<Rectangle>(p, filledColor, strokeColor, strokeThickness);
		case Tool::DRAW_REGULAR_POLYGONE:
			ofLog() << "Create regular polygone";
			return make_shared<RegularPolygone>(p, nbOfPoint, filledColor, strokeColor, strokeThickness);
		default:
			ofLog() << "nothing created";
			return nullptr;
	}
}


bool Primitive::isInside(ofPoint& p)
{
	return boundingBox.inside(p);
}

bool Primitive::Rotate(float az, const ofVec3f &axis) 
{
	primitive.rotate(az, axis);
	this->angle += az;
	this->rotation = axis;
	return true;
}

bool Primitive::Translate(const ofPoint &p) 
{
	primitive.translate(p);
	boundingBox.translate(p);
	this->position = p;
	return true;
}

bool Primitive::MoveTo(const ofPoint &p) 
{
	primitive.moveTo(p);
	this->position = p;
	return true;
}

bool Primitive::MoveTo(float x, float y, float z)
{
	primitive.moveTo(x, y, z);
	this->position = ofPoint(x, y, z);
	return true;
}

void Primitive::Draw()
{
	this->primitive.setFillColor(filledColor);
	this->primitive.setStrokeColor(strokeColor);
	this->primitive.setStrokeWidth(strokeThickness);
	this->primitive.draw();
}

void Primitive::DrawBoundingBox() 
{
	ofPath tmp = ofPath();
	tmp.setFilled(false);
	tmp.setStrokeWidth(.5f);
	tmp.rectangle(boundingBox);
	tmp.draw();
}

void Primitive::UpdateBoundingBox()
{
	this->boundingBox = ofRectangle((this->minPoint), this->maxPoint.x - this->minPoint.x, this->maxPoint.y - this->minPoint.y);
}

std::string Primitive::GetName() const 
{
	return name;
}

void Primitive::SetName(std::string name)
{
	this->name = name;
}

void Primitive::SetFilledColor(ofColor filledColor)
{
	this->filledColor = filledColor;
	this->primitive.setFillColor(this->filledColor);
}

void Primitive::SetStrokeColor(ofColor strokeColor)
{
	this->strokeColor = strokeColor;
	this->primitive.setStrokeColor(this->strokeColor);
}

void Primitive::SetStrokeThickness(int thickness)
{
	this->strokeThickness = thickness;
	this->primitive.setStrokeWidth(this->strokeThickness);
}

ofColor Primitive::GetFilledColor()
{
	return this->filledColor;
}

ofColor Primitive::GetStrokeColor()
{
	return this->strokeColor;
}

int Primitive::GetStrokeThickness()
{
	return this->strokeThickness;
}

int Primitive::GetNumbrOfVertices()
{
	return 0;
}

Primitive& Primitive::operator=(const Primitive &p)
{
	this->name = p.name;
	this->primitive = p.primitive;
	this->position = p.position;
	this->rotation = p.rotation;
	this->angle = p.angle;
	return *this;
}

Primitive::Primitive(const Primitive &p)
	:name(p.name), rotation(p.rotation), angle(p.angle), position(p.position)
{
	this->primitive = p.primitive;
}

Primitive::Primitive(ofPoint &p, ofColor filledColor, ofColor strokeColor, int thickness)
	:filledColor(filledColor), strokeColor(strokeColor), strokeThickness(thickness), rotation(ofVec3f()), angle(0), position(p)
{
	this->primitive.setFillColor(filledColor);
	this->primitive.setStrokeColor(strokeColor);
	this->primitive.setStrokeWidth(thickness);
}

Primitive::Primitive(ofPoint &position, ofColor filledColor, ofColor strokeColor, int thickness, std::string name)
	:name(name), filledColor(filledColor), strokeColor(strokeColor), strokeThickness(thickness), rotation(ofVec3f()), angle(0), position(position)
{
	this->primitive.setFillColor(filledColor);
	this->primitive.setStrokeColor(strokeColor);
	this->primitive.setStrokeWidth(thickness);
}
