#pragma once

#include "Form3D.h"

namespace infographie
{
	class Box : public Form3D
	{
	private:
		//ofBoxPrimitive primitive;
		int width, height, depth;

		static int nbOfBox;
		const string BASE_NAME = "Box ";

	public:
		//virtual void draw() override;

		Box();
		Box(int, string = "");
		virtual ~Box();

		// Inherited via Form3D
		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e) override;
	};

}
