#include "Polyline.h"

using namespace infographie;

int Polyline::nbOfPolyline = 0;

void Polyline::AddPoint(ofPoint& pts) 
{
	ofLog() << "add Point to Polyline " << name;
	this->ptsList.push_back(pts);
	this->primitive.lineTo(pts);

	this->maxPoint.x = (this->maxPoint.x < pts.x) ? pts.x : this->maxPoint.x;
	this->maxPoint.y = (this->maxPoint.y < pts.y) ? pts.y : this->maxPoint.y;
	this->minPoint.x = (this->minPoint.x > pts.x) ? pts.x : this->minPoint.x;
	this->minPoint.y = (this->minPoint.y > pts.y) ? pts.y : this->minPoint.y;
	UpdateBoundingBox();
}


Polyline::Polyline(ofPoint& p, ofColor filledColor, ofColor strokeColor, int thickness, std::string name)
	:Primitive(p, filledColor, strokeColor, thickness)
{
	ptsList = vector<ofPoint>();
	if (name.compare("") == 0)
	{
		name = BASE_NAME + to_string(nbOfPolyline++);
	}
	this->name = name;
	this->ptsList.push_back(p);
	this->primitive.setMode(ofPath::POLYLINES);
	this->primitive.setFilled(false);
	this->primitive.moveTo(p);
	this->minPoint = ofPoint(p);
	this->maxPoint = ofPoint(p);
}

void Polyline::ComputeUsefullValue(ofPoint &p, bool isShiftPressed)
{

}

void Polyline::SetOfPathValue()
{

}

Polyline::~Polyline()
{
}
