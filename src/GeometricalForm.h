#pragma once
#include <map>
#include <string>
#include <utility>
#include "Rectangle.h"
#include "Polyline.h"

namespace infographie
{
	class GeometricalForm
	{
	private:
		std::map<std::string, Primitive*> primitives;
		float angleRotation;
		ofVec3f rotationAxis;
		ofPoint translationVector, position;

	public:
		bool RemovePrimitive(const Primitive &p);
		bool AddPrimitive(Primitive &p);
		bool Rotate(float az, const ofVec3f &axis);
		bool Translate(const ofPoint &p);
		bool MoveTo(const ofPoint &p);
		bool MoveTo(float, float, float);

		void Draw();

		GeometricalForm();
		GeometricalForm(ofPoint &p);
		virtual ~GeometricalForm();
	};
}

