#pragma once

#include "Form3D.h"

namespace infographie
{
	class Cone : public Form3D
	{
	private:
		static int nbOfCone;
		const string BASE_NAME = "Cone ";

	protected:
		float height, radius;
		int radiusSegment, heightSegment;


	virtual void PrimitiveCreation();

	public:
		Cone();
		Cone(int, string = "");
		virtual ~Cone();

		// Inherited via Form3D
		virtual void onCreatorButtonEvent(ofxDatGuiButtonEvent e);
	};
}

