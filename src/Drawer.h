#pragma once

#include "ofxDatGui\ofxDatGui.h"
#include "Utils.h"

namespace infographie
{
	class Drawer
	{
	protected:
		shared_ptr<AppState> appState;
		bool isSetup = false;

	public:

	protected:

	public:
		bool IsSetup();

		void SetAppState(shared_ptr<AppState>);

		virtual void MousePressed(ofPoint) = 0;
		virtual void MouseReleased(ofPoint) = 0;

		virtual bool DrawSelectorSquare() = 0;

		virtual void isEnable(bool) = 0;

		virtual void removeSelected() = 0;

		virtual void update() = 0;
		virtual void draw() = 0;
		virtual void setup() = 0;
		virtual void reset() = 0;

		virtual void SetupGUI() = 0;

		virtual void Load(std::string) = 0;
		virtual void Save(std::string) = 0;

		Drawer();
		virtual ~Drawer();
	};
}

