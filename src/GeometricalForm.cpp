#include "GeometricalForm.h"

using namespace infographie;

bool GeometricalForm::RemovePrimitive(const Primitive &p)
{
	std::map<const std::string, Primitive*>::iterator it;

	it = primitives.find(p.GetName());
	if (it != primitives.end())
	{
		primitives.erase(it);
	}
	return true;
}

bool GeometricalForm::AddPrimitive(Primitive &p) 
{
	*primitives[p.GetName()] = p;
	return true;
}

bool GeometricalForm::Rotate(float az, const ofVec3f &axis)
{
	angleRotation = az;
	rotationAxis = axis;
	return true;
}

bool GeometricalForm::Translate(const ofPoint &p)
{
	translationVector = p;
	return true;
}


bool GeometricalForm::MoveTo(const ofPoint &p)
{
	translationVector = p - position;
	return true;
}

bool GeometricalForm::MoveTo(float x, float y, float z)
{
	return true;
}

void GeometricalForm::Draw()
{
	ofPopMatrix();
	ofRotate(angleRotation, rotationAxis.x, rotationAxis.y, rotationAxis.z);
	ofTranslate(translationVector);
	for (std::pair<const std::string, Primitive*> p : primitives) p.second->Draw();
	ofPushMatrix();
}


GeometricalForm::GeometricalForm()
{
	primitives = map<std::string, Primitive*>();
	position = ofPoint();
}

GeometricalForm::GeometricalForm(ofPoint &p)
{
	primitives = map<std::string, Primitive*>();
	position = p;
}

GeometricalForm::~GeometricalForm()
{
}
